<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:50
 */

class Trainings {
    
    private static $_table = 'trainings';
    
    public static function GetAll(){
        $sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY date");

        return $sth->fetchAll();
    }

    public static function Get($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        return $sth->fetch();
    }

    public static function Add(){
        $id = 1;
        $post = $_POST['Trainings'];

        list($d, $m, $y) = explode('.', $post['date_start']);
        $post['date_start'] = mktime(0, 0, 0, $m, $d, $y);

        if($sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY id DESC LIMIT 1")){
            if($Trainings = $sth->fetch())
                $id =  $Trainings['id'] + 1;
            else
                $id = 1;
        }

        if(isset($post['status'])) $post['status'] = 1; else  $post['status'] = 0;

        $post['date'] = time();


        if(!empty($_FILES['img']['tmp_name'])){
            $file = $_FILES['img']['tmp_name'];
            $directory = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/trainings/';
            if(!is_dir($directory)) mkdir($directory, 0777, true);
            $fname = 'Trainings_'.$id.'.jpg';
            copy($file, $directory.$fname);
//            copy($file, $directory.'small_'.$fname);

            $img = new Picture($directory.$fname);
            $img->autoimageresize(600, 600);
            $img->imagesave($img->image_type, $directory.'small_'.$fname);
            $img->imageout();

            $post['img'] = $fname;
        }

        DB::make("INSERT INTO  ".self::$_table." ".DB::insertFieldsByArr($post), $post);
    }

    public static function Update($id){
        $post = $_POST['Trainings'];


        list($d, $m, $y) = explode('.', $post['date_start']);
        $post['date_start'] = mktime(0, 0, 0, $m, $d, $y);

        if(isset($post['status'])) $post['status'] = 1; else  $post['status'] = 0;

        if(!empty($_FILES['img']['tmp_name'])){
            $file = $_FILES['img']['tmp_name'];
            echo $file;
            $directory = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/trainings/';
            $fname = 'Trainings_'.$id.'.jpg';
            copy($file, $directory.$fname);

            $img = new Picture($directory.$fname);
            $img->autoimageresize(600, 600);
            $img->imagesave($img->image_type, $directory.'small_'.$fname);
            $img->imageout();


            $post['img'] = $fname;
        }
        $post['id'] = $id;

        DB::make("UPDATE ".self::$_table." SET ".DB::updateFieldsByArr($post)." WHERE id = :id", $post);
    }


    public static function Delete($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));


        if($n = $sth->fetch()){
            $dir = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/trainings/';
            @unlink($dir.$n['img']);
            @unlink($dir.'small_'.$n['image']);
            DB::make("DELETE FROM ".self::$_table." WHERE id = :id", array('id' => $id));
        }
    }
} 