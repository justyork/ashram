<?php
//------------------------------------------------------------------------
// Работа с почтой
//------------------------------------------------------------------------

class Mail{
	
	//--------------------------------------------------------------------
	// Отправка почты
	//--------------------------------------------------------------------
	/**
	* @( string )$to 		= Получатель письма
	* @( string )$subject	= Тема письма
	* @( string )$message 	= Сообщение
	* @( string )$from 		= Отправитель
	*
	* # bool	
	*/
	public static function send( $to, $subject, $message, $from ){ 
		$from = self::From( $from );
		$headers = self::Headers($from);
		$subject = self::Subject( $subject ); 
		
		if( mail( $to, $subject, $message, $headers ) ) 
			return true; 
		else 
			return false; 
	}
	
	//--------------------------------------------------------------------
	// Установка заголовков страницы
	//--------------------------------------------------------------------
	private static function Headers($from){
		$headers = "Content-type: text/plain; charset=\"utf-8\"\r\n"; 
		$headers .= "From: <". $from .">\r\n"; 
		$headers .= "MIME-Version: 1.0\r\n"; 
		$headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n"; 

		return $headers; 
	}
	
	//--------------------------------------------------------------------
	// Формирование темы письма
	//--------------------------------------------------------------------
	private static function Subject( $subject ) { 
       return '=?utf-8?b?'. base64_encode( $subject ) .'?='; 
	}	 
	
	//--------------------------------------------------------------------
	// Отправитель письма
	//--------------------------------------------------------------------
	private static function From( $from ) 
	{ 
		return trim( preg_replace( '/[\r\n]+/', ' ', $from ) ); 
	} 

	// Если нужно показать лог SMTP-сессии, то можно раскомментировать следующую строчку.
	//$_SERVER['debug'] = true;

	public static function MailSmtp($reciever, $subject, $content, $headers, $debug = 0) {

		$smtp_server = 'mail.web-y.ru'; // адрес SMTP-сервера
		$smtp_port = 25; // порт SMTP-сервера
		$smtp_user = 'vmail'; // Имя пользователя для авторизации на SMTP-сервере
		$smtp_password = 'jrLNb3g8p2'; // Пароль для авторизации на SMTP-сервере
		$mail_from = 'noreply@web-y.ru'; // Ящик, с которого отправляется письмо
	
		$sock = fsockopen($smtp_server,$smtp_port,$errno,$errstr,30);
	
		$str = fgets($sock,512);
		if (!$sock) {
			printf("Socket is not created\n");
			exit(1);
		}

		self::smtp_msg($sock, "HELO " . $_SERVER['SERVER_NAME']);
		self::smtp_msg($sock, "AUTH LOGIN");
		self::smtp_msg($sock, base64_encode($smtp_user));
		self::smtp_msg($sock, base64_encode($smtp_password));
		self::smtp_msg($sock, "MAIL FROM: <" . $mail_from . ">");
		self::smtp_msg($sock, "RCPT TO: <" . $reciever . ">");
		self::smtp_msg($sock, "DATA");

		$headers = "Subject: " . $subject . "\r\n" . $headers;

		$data = $headers . "\r\n\r\n" . $content . "\r\n.";

		self::smtp_msg($sock, $data);
		self::smtp_msg($sock, "QUIT");

		fclose($sock);
	}


	private static function smtp_msg($sock, $msg) {

		if (!$sock) {
			printf("Broken socket!\n");
			exit(1);
		}

		if (isset($_SERVER['debug']) && $_SERVER['debug']) {
			printf("Send from us: %s<BR>", nl2br(htmlspecialchars($msg)));
		}
		fputs($sock, "$msg\r\n");
		$str = fgets($sock, 512);
		if (!$sock) {
			printf("Socket is down\n");
			exit(1);
		}
		else {
			if (isset($_SERVER['debug']) && $_SERVER['debug']) {
				printf("Got from server: %s<BR>", nl2br(htmlspecialchars($str)));
			}

			$e = explode(" ", $str);
			$code = array_shift($e);
			$str = implode(" ", $e);

			if ($code > 499) {
				printf("Problems with SMTP conversation.<BR><BR>Code %d.<BR>Message %s<BR>", $code, $str);
				exit(1);
			}
		}
	} 
	
}
