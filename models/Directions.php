<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 13:48
 */

class Directions {

    private static $_table = 'directions';

    public static function GetAll($status = false){

        if($status)
            $sth = DB::query("SELECT * FROM ".self::$_table." WHERE status = 1 ORDER BY pos");
        else
            $sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY name");


        return JL::ListData($sth->fetchAll());
    }

    public static function Get($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        return $sth->fetch();
    }

    public static function Add(){
        $id = 1;
        $post = $_POST['Dir'];

        if($sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY id DESC LIMIT 1")){
            if($dir = $sth->fetch())
                $id =  $dir['id'] + 1;
            else
                $id = 1;
        }


        if(isset($post['bg'])) $post['bg'] = 1; else  $post['bg'] = 0;
        if(isset($post['status'])) $post['status'] = 1; else  $post['status'] = 0;

        if(!empty($_FILES['img']['tmp_name'])){
            $file = $_FILES['img']['tmp_name'];
            $directory = ROOT_PATH.'/assets/upload/';
            $fname = 'dir_'.$id.'.jpg';
            copy($file, $directory.$fname);
            $post['img'] = $fname;
        }

        DB::make("INSERT INTO  ".self::$_table." ".DB::insertFieldsByArr($post), $post);
    }

    public static function Update($id){
        $post = $_POST['Dir'];

        if(isset($post['bg'])) $post['bg'] = 1; else  $post['bg'] = 0;
        if(isset($post['status'])) $post['status'] = 1; else  $post['status'] = 0;

        if(!empty($_FILES['img']['tmp_name'])){
            $file = $_FILES['img']['tmp_name'];
            $directory = ROOT_PATH.'/assets/upload/';
            $fname = 'dir_'.$id.'.jpg';
            copy($file, $directory.$fname);
            $post['img'] = $fname;
        }
        $post['id'] = $id;

        DB::make("UPDATE ".self::$_table." SET ".DB::updateFieldsByArr($post)." WHERE id = :id", $post);
    }
	
	public static function Delete($id){
		DB::make("DELETE FROM directions WHERE id = :id", array('id' => $id));
		@unlink(ROOT_PATH.'/assets/upload/dir_'.$id.'.jpg');
	}
} 