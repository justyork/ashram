<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.10.2015
 * Time: 21:43
 */

class DB {

    public static function GetOne($q, $params){
        $sth = self::query($q, $params);
        return $sth->fetch();

    }
    public static function query($q, $params = false){
        global $DB, $DBUG;

        $DBUG['query'][] = Debug::PDOQuery($q, $params);
        if(!$params)
            $sth = $DB->query($q);
        else{
            $sth = $DB->prepare($q);
            $sth->execute($params);

        }
        return $sth;
    }

    public static function make($q, $params = false){
        global $DB, $DBUG;

        $DBUG['query'][] = Debug::PDOQuery($q, $params);

        $sth = $DB->prepare($q);
        if(!$params)
            $sth->execute();
        else
            $sth->execute($params);
    }
	public static function Insert($table, $arr){
        return self::make("INSERT INTO {$table} ".self::insertFieldsByArr($arr), $arr);
    }
    public static function Update($table, $arr){
        return self::make("UPDATE {$table} SET " . self::updateFieldsByArr($arr) . " WHERE id = :id", $arr);
    }
    public static function ByPk($table, $id){
        return self::GetOne("SELECT * FROM {$table} WHERE id = :id", array('id' => $id));
    }
    public static function Select($q, $params = false){
        $sth = self::query($q, $params);
        return $sth->fetchAll();
    }
    public static function Delete($table, $id){
        self::make("DELETE FROM {$table} WHERE id = :id", array('id' => $id));
    }
    public static function Count($q, $params = false){
//        $data = self::Select($q, $params);
        $data = self::GetOne($q, $params);
        return $data[0];
    }

	
	 public static function whereFieldsByArr($arr){
        $ret = array();
        foreach($arr as $key => $val){
            if($key == 'id') continue;
            $ret[] = "{$key} = :{$key}";
        }

        return ' '.implode(' AND ', $ret).' ';
    }
	
    public static function updateFieldsByArr($arr){
        $ret = array();
        foreach($arr as $key => $val){
            if($key == 'id') continue;
            $ret[] = "{$key} = :{$key}";
        }

        return ' '.implode(', ', $ret).' ';
    }
    public static function insertFieldsByArr($arr){
        $ret = array();
        foreach($arr as $key => $val){
            $ret['insert'][] = "{$key}";
            $ret['val'][] = ":{$key}";
        }


        return "(".implode(',', $ret['insert']).") VALUES (".implode(',', $ret['val']).")";
    }
} 