<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 13:48
 */

class Prep {
    private static $_table = 'prep';

    public static function GetAll(){
        $sth = DB::query("SELECT * FROM ".self::$_table."");

        return JL::ListData($sth->fetchAll());
    }

    public static function Get($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        return $sth->fetch();
    }

    public static function Add(){
        $id = 1;
        $post = $_POST['Prep'];

        if($sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY id DESC LIMIT 1")){
            if($dir = $sth->fetch())
                $id =  $dir['id'] + 1;
            else
                $id = 1; 
        }

        if(isset($post['status'])) $post['status'] = 1; else  $post['status'] = 0;

        if(!empty($_FILES['img']['tmp_name'])){
            $file = $_FILES['img']['tmp_name'];
            $directory = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/';
            $fname = 'prep_'.$id.'.jpg';
            copy($file, $directory.$fname);


            $img = new Picture($directory.$fname);
            if($img->image_width < $img->image_height)
                $img->imageresizewidth(600);
            else
                $img->imageresizeheight(600);

            $img->imagesave($img->image_type, $directory.'small_'.$fname);
            $img->imageout();

            JL::crop($directory.'small_'.$fname, $directory.'crop_'.$fname);


            $post['img'] = $fname;
        }

        DB::make("INSERT INTO  ".self::$_table." ".DB::insertFieldsByArr($post), $post);
    }

    public static function Update($id){
        $post = $_POST['Prep'];

        if(isset($post['status'])) $post['status'] = 1; else  $post['status'] = 0;

        if(!empty($_FILES['img']['tmp_name'])){
            $file = $_FILES['img']['tmp_name'];
            $directory = $_SERVER['DOCUMENT_ROOT'].'/assets/upload/';
            $fname = 'prep_'.$id.'.jpg';
            copy($file, $directory.$fname);

            $img = new Picture($directory.$fname);
            if($img->image_width < $img->image_height)
                $img->imageresizewidth(600);
            else
                $img->imageresizeheight(600);

            $img->imagesave($img->image_type, $directory.'small_'.$fname);
            $img->imageout();

            JL::crop($directory.'small_'.$fname, $directory.'crop_'.$fname);

            $post['img'] = $fname;
        }
        $post['id'] = $id;

        DB::make("UPDATE ".self::$_table." SET ".DB::updateFieldsByArr($post)." WHERE id = :id", $post);
    }

    public static function Delete($id)
    {
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        if($sth->fetch())
            DB::make("DELETE FROM ".self::$_table." WHERE id = :id", array('id' => $id));
    }
} 