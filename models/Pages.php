<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:50
 */

class Pages {

    private static $_table = 'pages';

    public static function GetAll(){
        $sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY date DESC");

        return $sth->fetchAll();
    }

    public static function Get($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        return $sth->fetch();
    }

    public static function Add(){
        $id = 1;
        $post = $_POST['Pages'];

        if($sth = DB::query("SELECT * FROM ".self::$_table." ORDER BY id DESC LIMIT 1")){
            if($Pages = $sth->fetch())
                $id =  $Pages['id'] + 1;
            else
                $id = 1;
        }

        $post['date'] = time();


        DB::make("INSERT INTO  ".self::$_table." ".DB::insertFieldsByArr($post), $post);
    }

    public static function Update($id){
        $post = $_POST['Pages'];

        $post['id'] = $id;

        DB::make("UPDATE ".self::$_table." SET ".DB::updateFieldsByArr($post)." WHERE id = :id", $post);
    }

    public static function Delete($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        if($sth->fetch())
            DB::make("DELETE FROM ".self::$_table." WHERE id = :id", array('id' => $id));
    }
} 