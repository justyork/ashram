<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.11.2015
 * Time: 11:34
 */

class Sched {
    private static $_table = 'schedule';

    public static function Add(){

        foreach($_POST['sched'] as $key => $val){
            $keys[] = '`'.$key.'`';
        }

		
		$vals = [];
        foreach($_POST['sched']['day'] as $k => $days){
			
			$check = "SELECT * FROM schedule WHERE time = :time AND duration = :duration AND day = :day AND id_dir = :id_dir";
			$carr = [
				'id_dir' => $_POST['sched']['id_dir'],
				'time' => $_POST['sched']['time'].':00',
				'duration' => $_POST['sched']['duration'],
				'day' => $k,
			];
			
			// var_dump($carr);
			$shed = DB::GetOne($check, $carr);
			// var_dump($shed);
			if($shed) continue;
			
			
            foreach($_POST['sched'] as $key => $val){
                if($key == 'day') $vals[$k][] = "'".$k."'";
                else $vals[$k][] = "'".$val."'";
            }
        }
		
		if(!$vals) return false;
		// die();
        $ret_val = array();
        foreach($vals as $val)
            $ret_val[] = "(".implode(',', $val).")";


		
        $q = "INSERT INTO schedule (".implode(',', $keys).") VALUES ".implode(',', $ret_val); 
        DB::make($q);

    }


    public static function GetAll(){
        $sth = DB::query("SELECT * FROM schedule ". self::$_table." ORDER BY `time`");
        return $sth->fetchAll();
    }

    public static function GetByDir(){
        $data = self::GetAll();

        $arr = array();
        foreach($data as $item){
            $arr[$item['id_dir']][$item['day']][] = substr($item['time'], 0, -3);
        }

        return $arr;

    }

    public static function Delete($id){
        $sth = DB::query("SELECT * FROM ".self::$_table." WHERE id = :id", array('id' => $id));

        if($sth->fetch())
            DB::make("DELETE FROM ".self::$_table." WHERE id = :id", array('id' => $id));
    }


} 