<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$title?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=ASSET_ADMIN_DIR?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=ASSET_ADMIN_DIR?>/css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/uikit.css"/>
    <link rel="stylesheet" href="/assets/css/uikit.gradient.css"/>
    <link rel="stylesheet" href="/assets/css/uikit.almost-flat.css"/>
    <link rel="stylesheet" href="/assets/css/components/datepicker.css"/>
    <link rel="stylesheet" href="/assets/css/components/datepicker.almost-flat.css"/>
    <link rel="stylesheet" href="/assets/css/components/datepicker.gradient.css"/>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?=ASSET_ADMIN_DIR?>/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=ASSET_ADMIN_DIR?>/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/">Панель администрирования</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/" target="_blank">На сайт</a></li>
                <li><a href="/admin/logout/">Выход</a></li>
            </ul>
            <?/*<form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>*/?>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="<?=$_GET['type'] == 'index' ? 'active' : ''?>"><a href="/admin/">Расписание</a></li>
<!--                <li class="--><?//=$_GET['type'] == 'schedule' ? 'active' : ''?><!--"><a href="/admin/schedule/">Расписание</a></li>-->
                <li class="<?=$_GET['type'] == 'directions' ? 'active' : ''?>"><a href="/admin/directions/">Направления</a></li>
                <li class="<?=$_GET['type'] == 'prep' ? 'active' : ''?>"><a href="/admin/prep/">Педагоги</a></li>
                <li class="<?=$_GET['type'] == 'news' ? 'active' : ''?>"><a href="/admin/news/">Новости</a></li>
                <li class="<?=$_GET['type'] == 'trainings' ? 'active' : ''?>"><a href="/admin/trainings/">Тренинги</a></li>
                <li class="<?=$_GET['type'] == 'pages' ? 'active' : ''?>"><a href="/admin/pages/">Страницы</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"> <?=$title?></h1>

            <?=$content?>

        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?=ASSET_ADMIN_DIR?>/js/bootstrap.min.js"></script>

<script src="/assets/js/uikit.js"></script>
<script src="/assets/js/components/datepicker.js"></script>




<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?=ASSET_ADMIN_DIR?>/js/ie10-viewport-bug-workaround.js"></script>




</body>
</html>
