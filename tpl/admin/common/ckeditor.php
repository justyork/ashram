<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.12.2015
 * Time: 16:30
 */
?>

<script src="/assets/libs/ckeditor/ckeditor.js"></script>
<script src="/assets/libs/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
    $(function(){
        $('.editor').each(function(){
            var id = $(this).attr('id');
            var editor = CKEDITOR.replace( id );
            CKFinder.setupCKEditor( editor, '/assets/libs/ckfinder/'
            ) ;
        })
    })
</script>