<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 13:56
 */
?>
<div>
    <a href="/admin/directions/" class="btn btn-info">&larr; Назад</a>
</div>
<hr/>
<form method="post" enctype="multipart/form-data" class="Dir_form">
    <div class="form-group">
        <label>Название</label>
        <input  class="form-control" type="text" name="Dir[name]" value="<?=JL::FormValue($data, 'Dir', 'name')?>" />
    </div>
    <div class="form-group">
        <label>Цена за месяц</label>
        <input class="form-control" type="text" name="Dir[price]" value="<?=JL::FormValue($data, 'Dir', 'price')?>" />
    </div>
    <div class="form-group">
        <label>Цена за занятие</label>
        <input class="form-control" type="text" name="Dir[price1]" value="<?=JL::FormValue($data, 'Dir', 'price1')?>" />
    </div>
    <div class="form-group">
        <label>Преподователь</label>
        <input  class="form-control" type="text" name="Dir[prep]" value="<?=JL::FormValue($data, 'Dir', 'prep')?>" />
    </div>
    <div class="form-group">
        <label>Анонс</label>
        <textarea class="form-control" style="height: 150px"  name="Dir[text]"><?=JL::FormValue($data, 'Dir', 'text')?></textarea>
    </div>
    <div class="form-group">
        <label>Текст</label>
        <textarea class="form-control ckeditor" id="dir_text" name="Dir[text_full]"><?=JL::FormValue($data, 'Dir', 'text_full')?></textarea>
    </div>
    <div class="form-group">
        <label for="dir_status">Активно</label>
        <input  type="checkbox" name="Dir[status]" id="dir_status" <?=JL::FormCb($data, 'Dir', 'status')?>  />
    </div>
    <div class="form-group">
        <label for="dir_bg">Синий фон</label>
        <input  type="checkbox" name="Dir[bg]" id="dir_bg" <?=JL::FormCb($data, 'Dir', 'bg')?>  />
    </div>
    <div class="form-group">
        <label>Изображение</label>
        <input  type="file" name="img"  />
        <?if(!empty($data['Dir']['img'])):?>
            <div style="padding: 10px;">
                <img src="/assets/upload/<?=$data['Dir']['img']?>?<?=JL::Random()?>"  style="height: 150px;"/>
            </div>
        <?endif?>
    </div>
    <div>
        <input  class="btn btn-success" type="submit" value="Сохранить" />
    </div>
</form>

<?=JL::renderPart('admin/common/ckeditor')?>