<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 13:46
 */
?>

<div>
    <a href="/admin/directions/add" class="btn btn-success">Добавить</a>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Название</th>
            <th>Педагог</th>
            <th>Активно</th>
            <th></th>
        </tr> 
    </thead>
    <?foreach($data['directions'] as $d):?>
        <tr>
            <td><a href="/admin/directions/edit/<?=$d['id']?>"><?=$d['name']?></a></td>
            <td><?=$d['prep']?></td>
            <td style="<?=$d['status'] == 1 ? 'background: rgba(0, 128, 0, 0.37)' : ''?>"><?=$d['status'] == 1 ? 'Да' : 'Нет'?></td>
			<td style="text-align: center;"><a href="/admin/directions/delete/<?=$d['id']?>" onclick="return confirm('Вы действительно хотите удалить это направление?');">Удалить</a></td>
        </tr>
    <?endforeach?>
</table>