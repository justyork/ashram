<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:51
 */

?>

<div>
    <a href="/admin/pages/add" class="btn btn-success">Добавить</a>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>Дата публикации</th>
        <th></th>
    </tr>
    </thead>
    <?foreach($data['pages'] as $d):?>
        <tr>
            <td><a href="/admin/pages/edit/<?=$d['id']?>"><?=$d['title']?></a></td>
            <td><?=date('d.m.Y H:d:i', $d['date'])?></td>
            <td><a onclick="return confirm('Вы действительно хотите удалить эту запись?')" href="/admin/pages/delete/<?= $d['id'] ?>">Удалить</a></td>
        </tr>
    <?endforeach?>
</table>