<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:51
 */

?>
<div>
    <a href="/admin/pages/" class="btn btn-info">&larr; Назад</a>
</div>
<hr/>
<form method="post" enctype="multipart/form-data" class="Pages_form">
    <div class="form-group">
        <label>Название</label>
        <input  class="form-control" type="text" name="Pages[title]" value="<?=JL::FormValue($data, 'Pages', 'title')?>" />
    </div>
    <div class="form-group">
        <label>Текст</label>
        <textarea class="form-control editor" id="page_text" name="Pages[text]"><?=JL::FormValue($data, 'Pages', 'text')?></textarea>
    </div>

    <div>
        <input  class="btn btn-success" type="submit" value="Сохранить" />
    </div>
</form>

<?=JL::renderPart('admin/common/ckeditor')?>