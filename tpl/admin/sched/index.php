<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 24.11.2015
 * Time: 11:39
 */ ?>


<form method="post" class="sched_from">
    <h1>Расписание</h1>
    <div>
        <label>Направление</label>
        <select name="sched[id_dir]">
            <?foreach($data['directions'] as $sch):?>
                <option value="<?=$sch['id']?>"><?=$sch['name']?></option>
            <?endforeach?>
        </select>
    </div>
    <div>
        <label>Время начала</label>
        <input type="time" name="sched[time]"/>
    </div>
    <div>
        <label>Длительность</label>
        <label><input type="radio" name="sched[duration]" value="30"/> 30 мин          </label>
        <label><input type="radio" name="sched[duration]" value="60" checked/> 1 час   </label>
        <label><input type="radio" name="sched[duration]" value="90"/> 1.5 часа        </label>
    </div>
    <div>
        <label>День недели</label>
        <label><input type="checkbox" name="sched[day][1]" value="1"/> Пн</label>
        <label><input type="checkbox" name="sched[day][2]" value="2"/> Вт</label>
        <label><input type="checkbox" name="sched[day][3]" value="3"/> Ср</label>
        <label><input type="checkbox" name="sched[day][4]" value="4"/> Чт</label>
        <label><input type="checkbox" name="sched[day][5]" value="5"/> Пт</label>
        <label><input type="checkbox" name="sched[day][6]" value="6"/> Сб</label>
        <label><input type="checkbox" name="sched[day][7]" value="7"/> Вс</label>
    </div>
    <div><input type="submit" value="Добавить"/></div>
</form>
<hr/>
<table class="table table-striped data_table">
    <tr>
        <th></th>
        <th>Пн</th>
        <th>Вт</th>
        <th>Ср</th>
        <th>Чт</th>
        <th>Пт</th>
        <th>Сб</th>
        <th>Вс</th>
    </tr>

    <?if(isset($data['sched'])):?>
        <?for($i = 0; $i < 23; $i++):$t = strlen($i) == 1 ? '0'.$i : $i;?>
            <tr><?=JL::renderPart('admin/sched/row', array('time' => $t.':00', 'hall' => $data['sched']));?></tr>
            <tr><?=JL::renderPart('admin/sched/row', array('time' => $t.':15', 'hall' => $data['sched']));?></tr>
            <tr><?=JL::renderPart('admin/sched/row', array('time' => $t.':30', 'hall' => $data['sched']));?></tr>
            <tr><?=JL::renderPart('admin/sched/row', array('time' => $t.':45', 'hall' => $data['sched']));?></tr>
        <?endfor?>
    <?endif?>
</table>