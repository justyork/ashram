<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:51
 */

?>

<div>
    <a href="/admin/trainings/add" class="btn btn-success">Добавить</a>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>Дата публикации</th>
        <th>Показывать</th>
        <th></th>
    </tr>
    </thead>
    <?foreach($data['trainings'] as $d):?>
        <tr>
            <td><a href="/admin/trainings/edit/<?=$d['id']?>"><?=$d['title']?></a></td>
            <td><?=date('d.m.Y H:d:i', $d['date'])?></td>
            <td style="<?=$d['status'] == 1 ? 'background: rgba(0, 128, 0, 0.37)' : ''?>"><?=$d['status'] == 1 ? 'Да' : 'Нет'?></td>
            <td><a onclick="return confirm('Вы действительно хотите удалить эту запись?')" href="/admin/trainings/delete/<?= $d['id'] ?>">Удалить</a></td>
        </tr>
    <?endforeach?>
</table>