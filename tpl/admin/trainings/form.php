<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:51
 */

?>
<div>
    <a href="/admin/trainings/" class="btn btn-info">&larr; Назад</a>
</div>
<hr/>
<form method="post" enctype="multipart/form-data" class="Trainings_form">
    <div class="form-group">
        <label>Название</label>
        <input  class="form-control" type="text" name="Trainings[title]" value="<?=JL::FormValue($data, 'Trainings', 'title')?>" />
    </div>
    <div class="form-group">
        <label>Начало тренинга</label>
        <input style="width: 200px;" data-uk-datepicker="{format:'DD.MM.YYYY'}" class="form-control" type="text" name="Trainings[date_start]" value="<?=JL::FormValueDate($data, 'Trainings', 'date_start')?>" />
    </div>
    <div class="form-group">
        <label>Текст</label>
        <textarea class="form-control" style="" name="Trainings[text]"><?=JL::FormValue($data, 'Trainings', 'text')?></textarea>
    </div>
    <div class="form-group">
        <label>Активно <input  type="checkbox" name="Trainings[status]" <?=JL::FormCb($data, 'Trainings', 'status')?>  /></label>

    </div>
    <div class="form-group">
        <label>Изображение</label>
        <input  type="file" name="img"  />
        <?if(!empty($data['Trainings']['img'])):?>
            <div style="padding: 10px;">
                <img src="/assets/upload/trainings/<?=$data['Trainings']['img']?>"  style="height: 150px;"/>
            </div>
        <?endif?>
    </div>
    <div>
        <input  class="btn btn-success" type="submit" value="Сохранить" />
    </div>
</form>