<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 13:46
 */
?>
<div>
    <a href="/admin/prep/add" class="btn btn-success">Добавить</a>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Имя</th>
            <th>Направление</th>
            <th>Активен</th>
            <th>Управление</th>
        </tr>
    </thead>
    <?foreach($data['prep'] as $d):?>
        <tr>
            <td><a href="/admin/prep/edit/<?=$d['id']?>"><?=$d['name']?></a></td>
            <td><?=$d['dir']?></td>
            <td style="<?=$d['status'] == 1 ? 'background: rgba(0, 128, 0, 0.37)' : ''?>"><?=$d['status'] == 1 ? 'Да' : 'Нет'?></td>
			<td><a href="/admin/prep/delete/<?=$d['id']?>">Удалить</a></td>
        </tr>
    <?endforeach?>
</table>