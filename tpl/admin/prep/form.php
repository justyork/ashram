<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 13:56
 */
?>
<div>
    <a href="/admin/prep/" class="btn btn-info">&larr; Назад</a>
</div>
<hr/>

<form method="post" enctype="multipart/form-data" class="dir_form">
    <div class="form-group">
        <label>Имя</label>
        <input class="form-control" type="text" name="Prep[name]" value="<?=JL::FormValue($data, 'Prep', 'name')?>" />
    </div>
    <div class="form-group">
        <label>Направление</label>
        <input class="form-control" type="text" name="Prep[dir]" value="<?=JL::FormValue($data, 'Prep', 'dir')?>" />
    </div>
    <div class="form-group">
        <label>Текст</label>
        <textarea class="form-control" style="width: 300px;height: 100px;" name="Prep[text]"><?=JL::FormValue($data, 'Prep', 'text')?></textarea>
    </div>
    <div class="form-group">
        <label>Активно</label>
        <input type="checkbox" checked name="Prep[status]" <?=JL::FormCb($data, 'Prep', 'status')?>  />
    </div>
    <div class="form-group">
        <label>Изображение</label>
        <input type="file" name="img"  />
        <?if(!empty($data['Prep']['img'])):?>
            <div style="padding: 10px;">
                <img src="/assets/upload/crop_<?=$data['Prep']['img']?>?<?=JL::Random()?>"  style="height: 150px;"/>
            </div>
        <?endif?>
    </div>
    <div>
        <input type="submit" value="Сохранить" />
    </div>
</form>