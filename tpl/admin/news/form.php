<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 07.11.2015
 * Time: 15:51
 */

?>

<div>
    <a href="/admin/news/" class="btn btn-info">&larr; Назад</a>
</div>
<hr/>
<form method="post" enctype="multipart/form-data" class="News_form">
    <div class="form-group">
        <label>Название</label>
        <input  class="form-control" type="text" name="News[title]" value="<?=JL::FormValue($data, 'News', 'title')?>" />
    </div>
    <div class="form-group">
        <label>Текст</label>
        <textarea class="form-control editor" id="news_text" name="News[text]"><?=JL::FormValue($data, 'News', 'text')?></textarea>
    </div>
    <div class="form-group">
        <label>Активно <input  type="checkbox" name="News[status]" <?=JL::FormCb($data, 'News', 'status')?>  /></label>

    </div>
    <div class="form-group">
        <label>Изображение</label>
        <input  type="file" name="image"  />
        <?if(!empty($data['News']['image'])):?>
            <div style="padding: 10px;">
                <img src="/assets/upload/news/<?=$data['News']['image']?>?<?=JL::Random()?>"  style="height: 150px;"/>
            </div>
        <?endif?>
    </div>
    <div>
        <input  class="btn btn-success" type="submit" value="Сохранить" />
    </div>
</form>

<?=JL::renderPart('admin/common/ckeditor')?>