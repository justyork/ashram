<!DOCTYPE html>
<html>
<head>
    
	<title><?=$this->title?></title>
    <?=JL::renderPart('front/common/head');?>
</head>
<body>

<?=JL::renderPart('front/common/header');?>
<section class="news" id="trainings">
    <div class="container">
        <div class="row">
            <h1><?=$data['page']['title']?></h1>
            <a href="/" style="margin-left: 5px;">&larr; Назад</a>
            <br/>
            <br/>
            <div class="clearfix"></div>
			<div id="owl-page-gallery" class="" style="text-align: center; margin-bottom: 20px;">
				<?for($i = 1; $i <= 15; $i++):?>
					<a href="/assets/front/img/inter/p<?=$i?>.jpg?<?=JL::Random()?>" rel="group" class="popup"><img style=""  src="/assets/front/img/inter/p<?=$i?>.jpg?<?=JL::Random()?>" /></a>
				<?endfor?>
			</div>
			<div class="clearfix"></div>
            <div id="" class="content">
                <div class="item">

                    <?=$data['page']['text']?>

                </div>
            </div>
        </div>
    </div>
</section>

<?=JL::renderPart('front/common/footer');?>
</body>
</html>