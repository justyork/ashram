<!DOCTYPE html>
<html>
<head>
    
	<title><?=$this->title?></title>
    <?=JL::renderPart('front/common/head');?>
</head>
<body>

<?=JL::renderPart('front/common/header');?>
<section class="news" id="news">
    <div class="container">
        <div class="row">
            <h1><?=$data['dir']['name']?></h1>
            <a href="/#block2">&larr; Назад</a>
            <br/>
            <br/>
            <div class="clearfix"></div>
            <div id="" class="" style="padding: 8px;">
                <div style="text-align: center;" class="dirImage"><img src="/assets/upload/<?= $data['dir']['img'] ?>?<?=JL::Random()?>" alt="" /></div>
                <p><?=$data['dir']['text_full']?></p>
            </div>
        </div>
    </div>
</section>
<?=JL::renderPart('front/common/footer');?>
</body>
</html>