<?
$dates = array();
foreach($data['trainings'] as $tr){
    if($tr['date_start'] < time()) continue;

    $m = (int)date('m', $tr['date_start']);
    $y = date('Y', $tr['date_start']);
    $dates[$m.'_'.$y] = array('m' => $m, 'y' => $y);
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?=$this->title?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="shortcut icon" href="favicon.png" />
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/bootstrap/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/bootiq/css/boutique.css">
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/fonts.css" />
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/main.css" />
    <link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/media.css" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>




</head>
<body>
<div class="left-menu">
    <div class="social-wrapper">
        <?=JL::renderPart('front/common/social');?>
        <a href="#" class="phone">503-505</a>
    </div>
    <?=JL::renderPart('front/common/menu');?>
</div>
<header class="mainhead" data-parallax="scroll" data-image-src="/assets/front/img/slide.jpg" data-z-index="1">
    <div class="container">
        <div class="row">
            <div class="social-wrapper">
                <?=JL::renderPart('front/common/social');?>
            </div>

            <span class="menu-mini"></span>
            <div class="block-header-center">
                <div class="logo"></div>
                <div class="descriptio-top"><p>Запишитесь на  первое занятие по любому</p>
                    <p>направлению со скидкой 50 %</p>
                </div>
                <a href="#inline" class="button-top popup_content">Отправить заявку</a>
            </div>
        </div>
    </div>
    <section class="navigation navigationmain">
        <div class="container">
            <div class="row">
                <div class="logos"></div>
                <?=JL::renderPart('front/common/menu');?>
                <a href="javascript:;" class="phone">503-505</a>
            </div>
        </div>
    </section>
</header>

<section class="block2" id="block2">
    <div class="container">
        <div class="row">
            <h1>Направления</h1>


            <?
            $total = count($data['directions']);
            $i = 1; foreach($data['directions'] as $dir):
                $even = $i % 2 == 0 ? true : false;

                $ni = $even ? $i - 1 : $i + 1;
                $ni2 = !$even ? $i - 1 : $i + 1;

                $last = $i == $total ? true : false;

                ?>
                <div class="dropdown <?=$dir['bg'] == 1 ? 'blue' : ''?>">
                    <div class="napravlenie-1" data-toggle="dropdown" aria-expanded="false">
                        <div class="napravlenie " style="background-image: url('/assets/upload/<?=$dir['img']?>?<?=JL::Random()?>')">
                            <p class="name-napravlenie"><?=$dir['name']?></p>
                            <p class="name-napravlenie-prepodovatel">ведет <?=$dir['prep']?></p>
                        </div>
                    </div>
                    <div class="block-description block-description1 dropdown-menu dropdown-menu-right <?=$even? 'dropdown-menu-right1' : ''?>" role="menu" aria-labelledby="menu1">
                        <span class="mfp-close close-dir">×</span>
                        <p><?=nl2br($dir['text'])?></p>
                        <p class="price">
                            <? /*if($dir['price'] != 0):?>от <span><?=$dir['price']?></span> руб/мес<?endif */?>
                            <?if($dir['price1'] != 0):?>от <span class="price_val"><?=$dir['price1']?></span> руб <span class="after_curr">/занятие</span><?endif?>
                        </p>
                        <div class="moreWrap">
                            <a href="/index/dir/<?=$dir['id']?>" class="button-more ">Подробнее</a>
                            <a href="/index/dir/<?=$dir['id']?>" class="button-more moreSmall">&rarr;</a>
                            <a href="#inline" class="button-top popup_content">Записаться</a>
                        </div>
                    </div>
                </div>



            <? $i++; endforeach?>


        </div>
    </div>
</section>
<section class="news" id="news">
    <div class="container">
        <div class="row">
            <h1>Новости</h1>
            <div id="owl-demo-news" class="owl-carousel owl-theme">
                <div class="item">
                    <? $news = $data['news']; ?>

                    <?=JL::renderPart('/front/index/news_row', array('news' => $news[0], 'h' => 3, 'class' => 'n-bl n-bl1')); ?>
                    <div class="n-bl close-wd">
                        <?=JL::renderPart('/front/index/news_row', array('news' => $news[1], 'h' => 4, 'class' => 'n-top n-top1')); ?>
                        <div class="n-top">
                            <?=JL::renderPart('/front/index/news_row', array('news' => $news[2], 'h' => 5, 'class' => 'left-bl')); ?>
                            <?=JL::renderPart('/front/index/news_row', array('news' => $news[3], 'h' => 5, 'class' => 'right-bl')); ?>
                        </div>
                    </div>

                </div>
                <div class="item">
                    <div class="n-bl">
                        <div class="n-top">
                <?$i = 1; foreach($data['news'] as $key => $new):
                    if($key < 4) continue;
                    $ev = $i != 0 && $i % 2 == 0 ? true : false;
                    $evn = $i != 0 && $i % 4 == 0 ? true : false;
                    $even = $i != 0 && $i % 8 == 0 ? true : false;

                    ?>
                            <?=JL::renderPart('/front/index/news_row', array('news' => $new, 'h' => 5, 'class' => $ev ? 'right-bl' : 'left-bl')); ?>
                            <?if($ev):?>
                                </div>
                                <?if($evn):?>
                                    </div>
                                        <?if($even):?></div><div class="item"><?endif?>
                                    <div class="n-bl <?=$even ? 'close-wd' :''?>">
                                <?endif?>
                                <div class="n-top ">
                            <?endif?>

                    <? $i++; endforeach?>

                        </div>
                    </div>

                </div>


            </div>

            <div id="owl-demo-news2" class="owl-carousel owl-theme">
                <div class="item">
                    <? $news = $data['news']; ?>

                    <?=JL::renderPart('/front/index/news_row', array('news' => $news[0], 'h' => 3, 'class' => 'n-bl n-bl1')); ?>
                </div>
                <div class="item">
                    <div class="n-bl close-wd">
                        <?=JL::renderPart('/front/index/news_row', array('news' => $news[1], 'h' => 4, 'class' => 'n-top n-top1')); ?>
                        <div class="n-top">
                            <?=JL::renderPart('/front/index/news_row', array('news' => $news[2], 'h' => 5, 'class' => 'left-bl')); ?>
                            <?=JL::renderPart('/front/index/news_row', array('news' => $news[3], 'h' => 5, 'class' => 'right-bl')); ?>
                        </div>
                    </div>

                </div>
                <div class="item">
                    <div class="n-bl">
                        <div class="n-top">
                            <?$i = 1; foreach($data['news'] as $key => $new):
                            if($key < 4) continue;
                            $ev = $i != 0 && $i % 2 == 0 ? true : false;
                            $evn = $i != 0 && $i % 4 == 0 ? true : false;
                            $even = $i != 0 && $i % 8 == 0 ? true : false;

                            ?>
                            <?=JL::renderPart('/front/index/news_row', array('news' => $new, 'h' => 5, 'class' => $ev ? 'right-bl' : 'left-bl')); ?>
                            <?if($ev):?>
                        </div>
                        <?if($evn):?>
                    </div>
                    <?if($even):?></div><div class="item"><?endif?>
                    <div class="n-bl <?=$even ? 'close-wd' :''?>">
                        <?endif?>
                        <div class="n-top ">
                            <?endif?>

                            <? $i++; endforeach?>

                        </div>
                    </div>

                </div>
            </div>
			<div class="mobile_news">
				<? $i = 0; foreach($data['news'] as $key => $new): if($i == 5) break;?>
					<?=JL::renderPart('/front/index/mobile_news_row', array('news' => $new)); ?> 
				<?$i++; endforeach?>
			</div>
        </div>
    </div>
</section>
<section class="table" id="raspisanie">
    <div class="container">
        <div class="row">
            <h1>Расписание</h1>
            <p class="mobile_sched_text">Нажмите на название курса, чтобы узнать время занятий</p>
            <table>
                <?$i = 0; foreach($data['directions'] as $dir):
                    if($dir['status'] == 0) continue;
                    $item = array();
                    if(isset($data['sched'][$dir['id']]))
                        $item = $data['sched'][$dir['id']];
                    if(empty($item)) continue;
                    ?>
                    <tr class="tr-1">
                        <td><?=$dir['name']?></td>
                        <?if(isset($item[1])):?><td class="day day1"><?=implode('<br />', $data['sched'][$dir['id']][1]);?></td><?else:?><td></td><?endif?>
                        <?if(isset($item[2])):?><td class="day day2"><?=implode('<br />', $data['sched'][$dir['id']][2]);?></td><?else:?><td></td><?endif?>
                        <?if(isset($item[3])):?><td class="day day3"><?=implode('<br />', $data['sched'][$dir['id']][3]);?></td><?else:?><td></td><?endif?>
                        <?if(isset($item[4])):?><td class="day day4"><?=implode('<br />', $data['sched'][$dir['id']][4]);?></td><?else:?><td></td><?endif?>
                        <?if(isset($item[5])):?><td class="day day5"><?=implode('<br />', $data['sched'][$dir['id']][5]);?></td><?else:?><td></td><?endif?>
                        <?if(isset($item[6])):?><td class="day day6"><?=implode('<br />', $data['sched'][$dir['id']][6]);?></td><?else:?><td></td><?endif?>
                        <?if(isset($item[7])):?><td class="day day7"><?=implode('<br />', $data['sched'][$dir['id']][7]);?></td><?else:?><td></td><?endif?>
                    </tr>
                <?$i++; endforeach?>

            </table>
            <div class="wrapp-mobile-table">
                <?$i = 1; foreach($data['directions'] as $dir):
                    if($dir['status'] == 0) continue;

                    $item = array();
                    if(isset($data['sched'][$dir['id']]))
                        $item = $data['sched'][$dir['id']];
                    if(empty($item)) continue;
                    ?>
                    <div class="t1">
                        <h2 class="raspisanie raspisanie<?=$i?>"><?=$dir['name']?></h2>
                        <div class="wrapp-week wrapp-week<?=$i?>">
                            <p class="week">        <?if(isset($item[1])):?><span>Пн</span><?=implode('<br />', $data['sched'][$dir['id']][1]);?><?endif?></p>
                            <p class="week week2">  <?if(isset($item[2])):?><span>Вт</span><?=implode('<br />', $data['sched'][$dir['id']][2]);?><?endif?></p>
                            <p class="week">        <?if(isset($item[3])):?><span>Ср</span><?=implode('<br />', $data['sched'][$dir['id']][3]);?><?endif?></p>
                            <p class="week week2">  <?if(isset($item[4])):?><span>Чт</span><?=implode('<br />', $data['sched'][$dir['id']][4]);?><?endif?></p>
                            <p class="week week2">  <?if(isset($item[5])):?><span>Пт</span><?=implode('<br />', $data['sched'][$dir['id']][5]);?><?endif?></p>
                            <p class="week">        <?if(isset($item[6])):?><span>Сб</span><?=implode('<br />', $data['sched'][$dir['id']][6]);?><?endif?></p>
                            <p class="week week2">  <?if(isset($item[7])):?><span>Вс</span><?=implode('<br />', $data['sched'][$dir['id']][7]);?><?endif?></p>
                            <p class="phone popup_content" href="#inline"></p>
                        </div>
                    </div>
                <?$i++; endforeach?>
            </div>
            <a href="#inline" class="button-top popup_content">Отправить заявку</a>
        </div>
    </div>
</section>
<section class="our" id="pedagogi" >
    <div class="container">
        <div class="row">
            <h1>Наши мастера</h1>
            <ul id="boutique">

                <?foreach( $data['prep'] as $prep):  ?>
                    <li class="popup_prep" rel="<?=$prep['id']?>">
					  
                        <img src="/assets/upload/crop_<?=$prep['img']?>?<?=JL::Random()?>">
                        <div class="img-sl">
                            <p class="tit"><?=$prep['name']?></p>
                            <p class="dt"><?=$prep['dir']?></p> 
                        </div>
                    </li>
                <?endforeach?>
            </ul>
			<div class="prep_nav">
				<a href="#" class="prep_nav_back" onclick="$('.popup_prep.frame2').click(); return false;">&larr; Назад</a>
				<a href="#" class="prep_nav_next" onclick="$('.popup_prep.frame4').click(); return false;">Вперед &rarr;</a>
			</div>
        </div>
    </div>
</section>
<?if(count($dates)):?>
	<section class="table" id="trainings">
		<div class="container">
			<div class="row">
				<h1>Тренинги и мастер классы</h1>
				<div class="clearfix"></div>

				<div class="month_nav">
					<? $i = 0; foreach($dates as $key =>  $d):?>
						<li rel="<?=$key?>" class="<?=$i == 0 ? 'active' : ''?> train_month_item"><?=JL::$monthFull[$d['m']]?> <?=$d['y']?></li>
						<? $i++; endforeach?>
					<div class="control">
						<span class="prev" style="display: none;"></span>
						<span class="next" style="<?=count($dates) > 1 ? '' : 'display: none;'?>"></span>

					</div>
				</div>
				<div>

					<? $i = 0; foreach($dates as $key =>  $d):?>
						<ul class="train_list list<?=$key?> <?=$i == 0 ? 'active' : ''?>" >
							<? $i = 0; foreach($data['trainings'] as $tr):
								if($tr['date_start'] < time()) continue;
								$m = (int)date('m', $tr['date_start']);
								$y = date('Y', $tr['date_start']);
								if($key != $m.'_'.$y) continue;
								$even = $i % 2 == 0 ? true : false;
								?>
								<li style="<?=$even ? 'background-color: rgba(194, 194, 194, 0.14)' : ''?>">
									<div class="train_title">
										<span class="train_date"><?=date('d.m', $tr['date_start'])?></span>
										<span class="train_name"><?=$tr['title']?></span>
									</div>
									<div class="train_text">
										<?if(!empty($tr['img'])):?>
											<img src="/assets/upload/trainings/small_<?= $tr['img'] ?>?<?=JL::Random()?>" alt=""/>
										<?endif?>
										<?= nl2br($tr['text'])?>

										<div class="clearfix"></div>
									</div>
								</li>
								<? $i++; endforeach?>
						</ul>
						<? $i++; endforeach?>

				</div>
			</div>
		</div>
	</section>
<?endif?>
<?=JL::renderPart('front/common/footer');?>
</body>
</html>