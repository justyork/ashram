<!DOCTYPE html>
<html>
<head>
	<title><?=$this->title?></title>
    <?=JL::renderPart('front/common/head');?>
</head>
<body>

<?=JL::renderPart('front/common/header');?>
<section class="news" id="news">
    <div class="container">
        <div class="row">
            <?

                $m = (int)date('m', $data['news']['date']);
                $m = mb_strtoupper(JL::$monthShort[$m], 'utf-8');
                $d = (int)date('d', $data['news']['date']);
            ?>
            <h1>Новости</h1>
            <a href="/#news" style="margin-left: 5px;">&larr; Назад</a>
            <br/>
            <br/>
            <div class="clearfix"></div>
            <div >
                <div class="item">
                    <div class="n-bl n-bl1"  style="background-image: url('/assets/upload/news/<?=$data['news']['image']?>?<?=JL::Random()?>')">
                        <span class="date"><?=$d?><br /><span><?=$m?></span></span>
                    </div>
                    <div class="n-bl n-bl-news">
                        <p class="title-news"><?=$data['news']['title']?></p>
                        <p><?=nl2br($data['news']['text'])?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?=JL::renderPart('front/common/footer');?>
</body>
</html>