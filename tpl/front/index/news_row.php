<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 03.12.2015
 * Time: 15:33
 */

$m = (int)date('m', $news['date']);
$m = mb_strtoupper(JL::$monthShort[$m], 'utf-8');
$d = (int)date('d', $news['date']);
?>

<a href="/news/get/<?=$news['id']?>/">
    <div class="<?=$class?>" style="background-image: url('/assets/upload/news/<?=$news['image']?>?<?=JL::Random()?>')">
        <span class="date"><?=$d;?><br /><span><?=$m?></span></span>
        <div class="text-block">
            <h<?=$h?>><?=$news['title']?></h<?=$h?>>
            <?if($h == 3):?>
                <p><?=JL::str_word($news['text'], 25)?></p>
            <?endif?>
        </div>
        <div class="in-wrp-na-hovers"></div>
    </div>
</a>