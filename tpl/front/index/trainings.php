<?
    $dates = array();
    foreach($data['trainings'] as $tr){
        if($tr['date_start'] < time()) continue;

        $m = (int)date('m', $tr['date_start']);
        $y = date('Y', $tr['date_start']);
        $dates[$m.'_'.$y] = array('m' => $m, 'y' => $y);
    }

?>
<!DOCTYPE html>
<html>
<head>
    
	<title><?=$this->title?></title>
    <?=JL::renderPart('front/common/head');?>
</head>
<body>

<?=JL::renderPart('front/common/header');?>
<section class="table" id="trainings">
    <div class="container">
        <div class="row">
              <h1>Тренинги и мастер классы</h1>
            <a style="margin-left: 5px;" href="/">&larr; Назад</a>
            <br/>
            <br/>
            <div class="clearfix"></div>

            <div class="month_nav">
                <? $i = 0; foreach($dates as $key =>  $d):?>
                    <li rel="<?=$key?>" class="<?=$i == 0 ? 'active' : ''?> train_month_item"><?=JL::$monthFull[$d['m']]?> <?=$d['y']?></li>
                <? $i++; endforeach?>
                <div class="control">
                    <span class="prev" style="display: none;"></span>
                    <span class="next" style="<?=count($dates) > 1 ? '' : 'display: none;'?>"></span>

                </div>
            </div>
            <div>

                <? $i = 0; foreach($dates as $key =>  $d):?>
                    <ul class="train_list list<?=$key?> <?=$i == 0 ? 'active' : ''?>" >
                        <? $i = 0; foreach($data['trainings'] as $tr):
                            if($tr['date_start'] < time()) continue;
                            $m = (int)date('m', $tr['date_start']);
                            $y = date('Y', $tr['date_start']);
                            if($key != $m.'_'.$y) continue;
                            $even = $i % 2 == 0 ? true : false;
                            ?>
                            <li style="<?=$even ? 'background-color: rgba(194, 194, 194, 0.14)' : ''?>">
                                <div class="train_title">
                                    <span class="train_date"><?=date('d.m', $tr['date_start'])?></span>
                                    <span class="train_name"><?=$tr['title']?></span>
                                </div>
                                <div class="train_text">
                                    <?if(!empty($tr['img'])):?>
                                        <img src="/assets/upload/trainings/small_<?= $tr['img'] ?>?<?=JL::Random()?>" alt=""/>
                                    <?endif?>
                                    <?= nl2br($tr['text'])?>

                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            <? $i++; endforeach?>
                    </ul>
                <? $i++; endforeach?>

            </div>
        </div>
    </div>
</section>
<?=JL::renderPart('front/common/footer');?>
</body>
</html>