<!DOCTYPE html>
<html>
<head>
	<title><?=$this->title?></title>
    <?=JL::renderPart('front/common/head');?>
</head>
<body>

<?=JL::renderPart('front/common/header');?>
<section class="" id="">
    <div class="container">
        <div class="row"> 
            <h1><?=$data['prep']['name']?></h1>
            <a href="/#pedagogi" style="margin-left: 5px;">&larr; Назад</a>
            <br/>
            <br/>
            <div class="clearfix"></div>
            <div >
                <div class=" ">
                    <div class="n-bl n-bl1" style="    display:inline-block; margin-right: 20px; margin-bottom: 20px; margin-left: 10px;"> 
						<img src="/assets/upload/<?=$data['prep']['img']?>?<?=JL::Random()?>" style="max-height: 400px;" />
                    </div>
                    <div class="n-bl n-bl-news" style="    display:inline-block; min-width: 300px;  margin-left: 10px; vertical-align: top;">
                        <p><?=nl2br($data['prep']['text'])?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?=JL::renderPart('front/common/footer');?>
</body>
</html>