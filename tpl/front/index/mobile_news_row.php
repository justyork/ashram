<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 03.12.2015
 * Time: 15:33
 */

$m = (int)date('m', $news['date']);
$m = mb_strtoupper(JL::$monthShort[$m], 'utf-8');
$d = (int)date('d', $news['date']);
?>

<div class="mobile_news_row">
	<a href="/news/get/<?=$news['id']?>/">
		
		<div class="mobile_image">
			<img src="/assets/upload/news/<?=$news['image']?>?<?=JL::Random()?>" style="height: 80px;" /> 
		</div>
		
		<div style="display: inline-block;" class="mobile_news_title">
			<span class="mobile_date"><?=$d;?> <span><?=$m?></span></span>
				<h5 style="color: black"><?=$news['title']?></h5> 
		</div>
		
	</a>
</div>