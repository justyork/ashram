<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 05.12.2015
 * Time: 20:47
 */
?>
<div class="left-menu">
    <div class="social-wrapper">
        <?=JL::renderPart('front/common/social');?>
        <a href="#" class="phone">503-505</a>
    </div>
    <?=JL::renderPart('front/common/menu');?>
</div>
<section class="navigation">
    <div class="container">
        <div class="row">
            <a href="/">
                <div class="logos"></div>
            </a>
            <?=JL::renderPart('front/common/menu');?>
            <a href="#" class="phone">503-505</a>
        </div>
    </div>
</section>
<header class="news-head" data-parallax="scroll" data-image-src="/assets/front/img/slide.jpg" data-z-index="1">
    <div class="container">
        <div class="row">
            <div class="social-wrapper social-wrapper-news">
                <?=JL::renderPart('front/common/social');?>
            </div>
            <span class="menu-mini"></span>
            <a href="/">
                <div class="logos"></div>
            </a>
            <div class="descriptio-top"><p>Запишитесь на  первое занятие по любому</p>
				<p>направлению со скидкой 50 %</p>
			</div>
            <a href="#inline" class="button-top popup_content">Отправить заявку</a>
            <div id="inline" class="mfp-hide">
                <div class="wrapper-z">
                    <p>Пожалуйста, оставьте свое имя и телефон,<br />и мы вам перезвоним</p>
                    <input type="text"  class="send_name"  placeholder="Введите ваше имя">
                    <input type="text"  class="send_phone" placeholder="Телефон">
                    <br /><br />
                    <a href="#"  onclick="sendRequest();" class="button-top">Заказать звонок</a>
                    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                </div>
            </div>
        </div>
    </div>
</header>
