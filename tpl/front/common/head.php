<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="favicon.png" />
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/bootstrap/bootstrap-grid.min.css" />
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/magnific-popup/magnific-popup.css" />
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/bootiq/css/boutique.css">
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/libs/owl-carousel/owl.theme.css">
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/fonts.css" />
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/main.css" />
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/news.css" />
<link rel="stylesheet" href="<?=ASSET_FRONT_DIR?>/css/media.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>