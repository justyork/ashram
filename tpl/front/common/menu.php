<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 05.12.2015
 * Time: 20:50
 */
 
$data['trainings'] = Trainings::GetAll();
$dates = array();
foreach($data['trainings'] as $tr){
    if($tr['date_start'] < time()) continue;

    $m = (int)date('m', $tr['date_start']);
    $y = date('Y', $tr['date_start']);
    $dates[$m.'_'.$y] = array('m' => $m, 'y' => $y);
}

 
?>


<ul>
    <li><a href="/#block2">Направления</a></li>
    <li><a href="/#news">Новости</a></li>
    <li><a href="/#pedagogi">Мастера</a></li>
    <li><a href="/#raspisanie">Расписание</a></li>
	<?if(count($dates)):?>
		<li><a href="/#trainings">Тренинги</a></li>
	<?endif?>
    <li><a href="/about/">О нас</a></li>
    <li><a href="#contacts">Контакты</a></li>
</ul>