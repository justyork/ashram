
<footer id="contacts">
    <div class="google-maps">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="wrapper-contacts">
                        <h1>Контакты</h1>
                        <p class="phone-c">503-505</p>
                        <p class="mails">503505@bk.ru</p>
                        <p class="adress"> г. Иркутск, ул. Горького 36 Б, Бизнес Центр  MAXIM, 7 этаж, офис 1-18</p>
                        <p>
                            <?=JL::renderPart('front/common/social');?>
                        </p>
                        <p class="text">Поможем определиться с выбором занятий
                            и ответим на все интересующие вас вопросы</p>
                        <a href="#" class="button-top">Заказать звонок</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-bottom-line">
            <div class="container">
                <div class="row">
                    <a href="/" class="school">© Школа медитации “Ashram”</a>
                    <p>Сделано в студии <a href="http://web-y.ru">“WebYard”</a></p>
                </div>
            </div>
        </div>
        <div id="map"></div>
        <script type="text/javascript">

            var map;
            function initMap() {
                var myLatLng, center;
                myLatLng = {lat: 52.284551, lng: 104.2862233};
                center = {lat: 52.284551, lng: 104.2852233};
                var width = $(window).width();
                if(width < 975)
                    center = {lat: 52.284556, lng: 104.284355};
                if(width < 777)
                    center = {lat: 52.284556, lng: 104.284855};
                if(width < 734)
                    center = {lat: 52.285556, lng: 104.286095};


                map = new google.maps.Map(document.getElementById('map'), {
                    center: center,
                    zoom: 18,
                    scrollwheel: false,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.RIGHT_CENTER
                    }
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'Школа медитации Ашрам'
                });


                var height = $('footer .google-maps').height();
                $('#map').css('height', height);


            }

        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBgrYJpzFYux-L01i-hIDDeIJ8spygZas&callback=initMap">
        </script>


</footer>
<div id="inline" class="mfp-hide">
    <div class="wrapper-z">
        <p>Пожалуйста, оставьте свое имя и телефон,<br />и мы вам перезвоним</p>
        <input type="text" class="send_name" placeholder="Введите ваше имя">
        <input type="text" class="send_phone" placeholder="Телефон">
        <br /><br />
        <a href="javascript:;"  onclick="sendRequest();" class="button-top">Заказать звонок</a>
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
    </div>
</div> 
<!--[if lt IE 9]>
<script src="<?=ASSET_FRONT_DIR?>/libs/html5shiv/es5-shim.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/html5shiv/html5shiv.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/respond/respond.min.js"></script>
<![endif]-->

<script src="<?=ASSET_FRONT_DIR?>/libs/jquery/jquery-2.1.3.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/parallax/parallax.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/scroll2id/PageScroll2id.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/bootiq/js/jquery.boutique.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/owl-carousel/owl.carousel.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/libs/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?=ASSET_FRONT_DIR?>/js/common.js"></script>
<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
<!-- Google Analytics counter --><!-- /Google Analytics counter -->