$(document).ready(function() {
    $("section.navigation ul a").mPageScroll2id({
        offset: 75
    });

    $( ".wrp-na-8" ).hover(function() {
        $(".wrp-na-8").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-7" ).hover(function() {
        $(".wrp-na-7").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-6" ).hover(function() {
        $(".wrp-na-6").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-5" ).hover(function() {
        $(".wrp-na-5").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-4" ).hover(function() {
        $(".wrp-na-4").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-3" ).hover(function() {
        $(".wrp-na-3").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-2" ).hover(function() {
        $(".wrp-na-2").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-1" ).hover(function() {

        $(".wrp-na-1").toggleClass("wrp-na-d-on-1");
    });
    $( ".wrp-na-8" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-8)', this).hide();
        });
        $( ".wrp-na-d-8" ).toggle();
    });
    $( ".wrp-na-8" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr8)', this).hide();
        });
        $(".in-wrp-na-hoverr8").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-7" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-7)', this).hide();
        });
        $( ".wrp-na-d-7" ).toggle();
    });
    $( ".wrp-na-7" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr7)', this).hide();
        });
        $(".in-wrp-na-hoverr7").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-6" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-6)', this).hide();
        });
        $( ".wrp-na-d-6" ).toggle();
    });

    $( ".wrp-na-6" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr6)', this).hide();
        });
        $(".in-wrp-na-hoverr6").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-5" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-5)', this).hide();
        });
        $( ".wrp-na-d-5" ).toggle();
    });
    $( ".wrp-na-5" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr5)', this).hide();
        });
        $(".in-wrp-na-hoverr5").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-4" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-4)', this).hide();
        });
        $( ".wrp-na-d-4" ).toggle();
    });
    $( ".wrp-na-4" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr4)', this).hide();
        })
        $(".in-wrp-na-hoverr4").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-3" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-3)', this).hide();
        });
        $( ".wrp-na-d-3" ).toggle();
    });
    $( ".wrp-na-3" ).click(function() {
        $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr3)', this).hide();
        $(".in-wrp-na-hoverr3").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-2" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-2)', this).hide();
        });
        $( ".wrp-na-d-2" ).toggle();
    });
    $( ".wrp-na-2" ).click(function() {
        $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverr2)', this).hide();
        $(".in-wrp-na-hoverr2").toggleClass("in-wrp-na-hoverr");
    });
    $( ".wrp-na-1" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.wrp-na-d:not(.wrp-na-d-1)', this).hide();
        });
        $( ".wrp-na-d-1" ).toggle();
    });
    $( ".wrp-na-1" ).click(function() {
        $('.in-wrp-na').each(function(){
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverrr)', this).hide();
            $('.in-wrp-na-hoverr:not(.in-wrp-na-hoverrr)', this).hide();
        });
        $(".in-wrp-na-hoverrr").toggleClass("in-wrp-na-hoverr");
    });
    $("#owl-demo").owlCarousel({

        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination : false,
        singleItem:true

        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });
    $("#owl-demo-news").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination : false,
        autoHeight : true,
        singleItem:true
    });
    $("#owl-demo-news2").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination : false,
        autoHeight : true,
        singleItem:true
    });
    $("#owl-page-gallery").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination : true,
        autoHeight : true,
        singleItem:true
    });


    var width = $(window).width();

    var size = 500;
    var marg = -50;
    if(width < 800) size = 450;
    if(width < 700) {
        size = 400;
        marg = -30;
    }
    if(width < 600) size = 350;
    if(width < 500) {
        size = 300;
        marg = -20
    }
    if(width < 380) size = 250;


    $('#boutique').boutique({
        container_width: 'auto',
        front_img_width: size,
        front_img_height: size,
        behind_opacity: 0.3,
        hovergrowth: 0,
        frames: 3, 
        front_topmargin: marg
    });
    function closePopup() {
        $.magnificPopup.close();
    }
    $(".popup").magnificPopup({type:"image"});
    $(".popup_content").magnificPopup({
        type:"inline",
        midClick: true,

        showCloseBtn: true,
        closeBtnInside: true,

    });
    $("body").on('click', '.popup_prep', function(){
        
        if($(this).hasClass('zoomed')){
			
            $('#popupPrep .wrapper-z .popTitle').html($(this).find('.tit').html());
            $('#popupPrep .wrapper-z .popText').html($(this).find('.prepInfo').html());

			window.location = '/prep/get/'+$(this).attr('rel')+'/';
			
            /*$.magnificPopup.open({
                items: {
                    src: $('#popupPrep'), // can be a HTML string, jQuery object, or CSS selector
                    type:"inline",
                    midClick: true,

                    showCloseBtn: true,
                    closeBtnInside: true,
                }
            })*/
        }
    })


    $(".raspisanie1").click(function() {
        $(".raspisanie:not(.raspisanie1)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week1)").removeClass("active");
        $(".wrapp-week1").toggleClass("active");
        $("h2.raspisanie1").toggleClass("raspisanie8act");
    });
    $(".raspisanie2").click(function() {
        $(".raspisanie:not(.raspisanie2)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week2)").removeClass("active");
        $(".wrapp-week2").toggleClass("active");
        $("h2.raspisanie2").toggleClass("raspisanie8act");
    });
    $(".raspisanie3").click(function() {
        $(".raspisanie:not(.raspisanie3)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week3)").removeClass("active");
        $(".wrapp-week3").toggleClass("active");
        $("h2.raspisanie3").toggleClass("raspisanie8act");
    });
    $(".raspisanie4").click(function() {
        $(".raspisanie:not(.raspisanie4)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week4)").removeClass("active");
        $(".wrapp-week4").toggleClass("active");
        $("h2.raspisanie4").toggleClass("raspisanie8act");
    });
    $(".raspisanie5").click(function() {
        $(".raspisanie:not(.raspisanie5)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week5)").removeClass("active");
        $(".wrapp-week5").toggleClass("active");
        $("h2.raspisanie5").toggleClass("raspisanie8act");
    });
    $(".raspisanie6").click(function() {
        $(".raspisanie:not(.raspisanie6)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week6)").removeClass("active");
        $(".wrapp-week6").toggleClass("active");
        $("h2.raspisanie6").toggleClass("raspisanie8act");
    });
    $(".raspisanie7").click(function() {
        $(".raspisanie:not(.raspisanie7)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week7)").removeClass("active");
        $(".wrapp-week7").toggleClass("active");
        $("h2.raspisanie7").toggleClass("raspisanie8act");
    });
    $(".raspisanie8").click(function() {
        $(".raspisanie:not(.raspisanie8)").removeClass("raspisanie8act");
        $(".wrapp-week:not(.wrapp-week8)").removeClass("active");
        $(".wrapp-week8").toggleClass("active");
        $("h2.raspisanie8").toggleClass("raspisanie8act");
    });
    $(".menu-mini").click(function() {
        $(".left-menu").toggleClass("left-menu-active");
    });
    function heightDetect() {
		
		if($(window).width() < 400){
			$(".news-head").css("height", $(window).height());
		}
		
        $(".mainhead").css("height", $(window).height());
    };
    heightDetect();
    $(window).resize(function() {
        heightDetect();
    });
	
	
	
    $(".block-description1").click(function(event){
        event.stopPropagation();
        var element = $(this);
        //Закрываем все блоки
        $(".napravlenie").removeClass('block-description');
        //Открываем ту которая нам нужна
        element.addClass('block-description');
    });

});
$(window).load(function() {

    $('.train_list li').click(function(){
        $('.train_list li').removeClass('train_selected');
        $(this).addClass('train_selected');
        $('.train_list li .train_text').hide();
        $(this).find('.train_text').fadeIn();
    });

    $('#trainings .control .next').click(function(){
        var active = $('.month_nav .active');
        var atr = active.attr('rel');

        active.hide().removeClass('active');
        var next = active.next().addClass('active').fadeIn();

        if(!next.next().hasClass('train_month_item')) $('#trainings .control .next').hide();
        $('#trainings .control .prev').fadeIn();

        var list = $('.list'+atr);
        list.next().fadeIn();
        list.hide();

    });

    $('#trainings .control .prev').click(function(){
        var active = $('.month_nav .active');
        var atr = active.attr('rel');

        active.hide().removeClass('active');
        var prev = active.prev().addClass('active').fadeIn();

        if(!prev.prev().hasClass('train_month_item')) $('#trainings .control .prev').hide();
        $('#trainings .control .next').fadeIn();

        var list = $('.list'+atr);
        list.prev().fadeIn();
        list.hide();

    });

    $('.close-dir').click(function(){

        console.log('click');
        $(".dropdown").removeClass('open');
    });
	
	
	$('.left-menu ul li a').click(function(){
		$('.left-menu').removeClass('left-menu-active');
	});
	var initialPoint;
	var finalPoint;
	var obj = document.getElementById('boutique');
	// obj.addEventListener('touchstart', function(event) {
		// if (event.targetTouches.length == 1) {
			// var myclick=event.targetTouches[0]; 
			
			// window.location = '/prep/get/'+$('.popup_prep.frame3').attr('rel')+'/';
		// }
	// }, false);
	// obj.addEventListener('touchstart', function(event) {
		// event.preventDefault();
		// event.stopPropagation();
		// initialPoint=event.changedTouches[0];
	// }, false);
	// obj.addEventListener('touchend', function(event) {
		// event.preventDefault();
		// event.stopPropagation();
		// finalPoint=event.changedTouches[0];
		// var xAbs = Math.abs(initialPoint.pageX - finalPoint.pageX);
		// var yAbs = Math.abs(initialPoint.pageY - finalPoint.pageY);
		// if (xAbs > 20 || yAbs > 20) {
			// if (xAbs > yAbs) {
				// if (finalPoint.pageX < initialPoint.pageX){
				
					// $('.popup_prep.frame4').click(); 
				// /*СВАЙП ВЛЕВО*/}
				// else{
					// $('.popup_prep.frame2').click();
				
				// /*СВАЙП ВПРАВО*/}
			// } 
		// }
	// }, false);
	
	
	
});
$(window).scroll(function(){

    if ($(window).scrollTop() > 700) {
        $('.navigationmain').addClass('scrolled');
    }
    else {
        $('.navigationmain').removeClass('scrolled')
    }
});

function sendRequest(){
    var name = $('.send_name').val();
    var phone = $('.send_phone').val();

    if(name == '' || phone == ''){
        alert('Заполните все поля');
        return;
    }

    $.post('/ajax/sendRequest/', {name:name, phone:phone}, function(o){
        if(o != 200){
            alert('Возникла ошибка попробуйте еще раз');
            return;
        }

        alert('Заявка успешно отправлена');

        $.magnificPopup.close();

    });

}