<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 11:25
 */

class C_Admin_Pages extends C_Admin_Base{
    protected function OnInput(){
        parent::OnInput();
        return $this->GeneratePage($this);
    }


    protected function OnOutput(){
        $vars = array( 'data' => $this->data );
        $this->content = $this->Template( $this->tpl, $vars);

        parent::OnOutput();
    }



    public function actionIndex(){

        $this->tpl = 'admin/pages/index';
        $this->title = 'Страницы';

        $this->data['pages'] = Pages::GetAll();
    }

    public function actionAdd(){

        $this->tpl = 'admin/pages/form';
        $this->title = 'Добавить страницу';

        if(isset($_POST['Pages'])){
            Pages::Add();

            JL::redirect('/admin/pages/');
        }

    }
    public function actionEdit(){
        $id = $_GET['id'];
        $this->tpl = 'admin/pages/form';
        $this->title = 'Редактировать страницу';

        if(isset($_POST['Pages'])){
            Pages::Update($id);

            JL::redirect('/admin/pages/edit/'.$id);
        }

        $this->data['Pages'] = Pages::Get($id);
    }

    public function actionDelete(){
        $id = $_GET['id'];

        Pages::Delete($id);
        JL::redirect('/admin/pages/edit/'.$id);
    }

} 