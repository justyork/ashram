<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 11:25
 */

class C_Admin_Prep extends C_Admin_Base{
    protected function OnInput(){
        parent::OnInput();
        return $this->GeneratePage($this);
    }


    protected function OnOutput(){
        $vars = array( 'data' => $this->data );
        $this->content = $this->Template( $this->tpl, $vars);

        parent::OnOutput();
    }


    public function actionIndex(){

        $this->tpl = 'admin/prep/index';
        $this->title = 'Преподаватели';

        $this->data['prep'] = Prep::GetAll();
    }

    public function actionAdd(){

        $this->tpl = 'admin/prep/form';
        $this->title = 'Добавить преподавателя';

        if(isset($_POST['Prep'])){
            Prep::Add();

            JL::redirect('/admin/prep/');
        }

    }
    public function actionEdit(){
        $id = $_GET['id'];
        $this->tpl = 'admin/prep/form';
        $this->title = 'Редактировать преподавателя';

        if(isset($_POST['Prep'])){
            Prep::Update($id);

            JL::redirect('/admin/prep/');
        }

        $this->data['Prep'] = Prep::Get($id);
    }

    public function actionDelete(){
        $id = $_GET['id'];
        Prep::Delete($id);
        JL::redirect('/admin/prep/');
    }

} 