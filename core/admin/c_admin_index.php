<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 11:25
 */

class C_Admin_Index extends C_Admin_Base{
    protected function OnInput(){
        parent::OnInput();
        return $this->GeneratePage($this);
    }


    protected function OnOutput(){
        $vars = array( 'data' => $this->data );
        $this->content = $this->Template( $this->tpl, $vars);

        parent::OnOutput();
    }


    public function actionIndex(){

        $this->tpl = 'admin/sched/index';
        $this->data['directions'] = Directions::GetAll();
        $sched = Sched::GetAll();

        foreach($sched as $h)
            $this->data['sched'][substr($h['time'], 0, 5)][$h['day']][] = array('sched' => $h, 'dir' => $this->data['directions'][$h['id_dir']]);


        if(isset($_POST['sched'])){
            if($_POST['sched']['time'] == '00:00:00')  JL::referer();
            if(!isset($_POST['sched']['day']))  JL::referer();

            Sched::Add();
            JL::referer();
        }
    }

    public function actionLogin(){
        if(isset($_POST['login'])){
            if(Auth::Login($_POST['login'], $_POST['password'])){
                JL::redirect('/admin/');
            }
        }
    }


    public function actionDelete(){
        $id = $_GET['id'];

        Sched::Delete($id);
        JL::redirect('/admin/');
    }

    public function actionLogout(){
        Auth::Logout();
        JL::redirect('/admin/');
    }

} 