<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 11:25
 */

class C_Admin_News extends C_Admin_Base{
    protected function OnInput(){
        parent::OnInput();
        return $this->GeneratePage($this);
    }


    protected function OnOutput(){
        $vars = array( 'data' => $this->data );
        $this->content = $this->Template( $this->tpl, $vars);

        parent::OnOutput();
    }



    public function actionIndex(){

        $this->tpl = 'admin/news/index';
        $this->title = 'Новости';

        $this->data['news'] = News::GetAll();
    }

    public function actionAdd(){

        $this->tpl = 'admin/news/form';
        $this->title = 'Добавить новость';

        if(isset($_POST['News'])){
            News::Add();

            JL::redirect('/admin/news/');
        }

    }
    public function actionEdit(){
        $id = $_GET['id'];
        $this->tpl = 'admin/news/form';
        $this->title = 'Редактировать новость';

        if(isset($_POST['News'])){
            News::Update($id);

            JL::redirect('/admin/news/edit/'.$id);
        }

        $this->data['News'] = News::Get($id);
    }

    public function actionDelete(){
        $id = $_GET['id'];
        News::Delete($id);
        JL::redirect('/admin/news/');
    }

} 