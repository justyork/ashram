<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 11:25
 */

class C_Admin_Trainings extends C_Admin_Base{
    protected function OnInput(){
        parent::OnInput();
        return $this->GeneratePage($this);
    }


    protected function OnOutput(){
        $vars = array( 'data' => $this->data );
        $this->content = $this->Template( $this->tpl, $vars);

        parent::OnOutput();
    }



    public function actionIndex(){

        $this->tpl = 'admin/trainings/index';
        $this->title = 'Тренинги и семинары';

        $this->data['trainings'] = Trainings::GetAll();
    }

    public function actionAdd(){

        $this->tpl = 'admin/trainings/form';
        $this->title = 'Добавить тренинг';

        if(isset($_POST['Trainings'])){
            Trainings::Add();

            JL::redirect('/admin/trainings/');
        }

    }
    public function actionEdit(){
        $id = $_GET['id'];
        $this->tpl = 'admin/trainings/form';
        $this->title = 'Редактировать тренинг';

        if(isset($_POST['Trainings'])){
            Trainings::Update($id);

            JL::redirect('/admin/trainings/edit/'.$id);
        }

        $this->data['Trainings'] = Trainings::Get($id);
    }

    public function actionDelete(){
        $id = $_GET['id'];
        Trainings::Delete($id);
        JL::redirect('/admin/trainings/');
    }

} 