<?php
/**
 * Created by PhpStorm.
 * User: York
 * Date: 06.11.2015
 * Time: 11:25
 */

class C_Admin_Directions extends C_Admin_Base{
    protected function OnInput(){
        parent::OnInput();
        return $this->GeneratePage($this);
    }


    protected function OnOutput(){
        $vars = array( 'data' => $this->data );
        $this->content = $this->Template( $this->tpl, $vars);

        parent::OnOutput();
    }
 

    public function actionIndex(){

        $this->tpl = 'admin/directions/index';
        $this->title = 'Направления';

        $this->data['directions'] = Directions::GetAll();
    }

    public function actionAdd(){

        $this->tpl = 'admin/directions/form';
        $this->title = 'Добавить направление';

        if(isset($_POST['Dir'])){
            Directions::Add();

            JL::redirect('/admin/directions/');
        }

    }
    public function actionEdit(){
        $id = $_GET['id'];
        $this->tpl = 'admin/directions/form';
        $this->title = 'Редактировать направление';

        if(isset($_POST['Dir'])){ 
            Directions::Update($id);

            JL::redirect('/admin/directions/');
        }

        $this->data['Dir'] = Directions::Get($id);
    }
	public function actionDelImg(){
		
        $id = $_GET['id'];
		
		unlink(ROOT_PATH.'/assets/upload/dir_'.$id.'.jpg');
	}
	public function actionDelete(){
		$id = $_GET['id'];
		
		$dir = Directions::Get($id);
		if($dir){
			Directions::Delete($id); 
		}
		JL::redirect('/admin/directions/');
		
	}
	

} 