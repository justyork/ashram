<?php
class C_Prep extends C_Base{
	protected function OnInput(){
        parent::OnInput();

        return $this->GeneratePage($this);
    }
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput(){

		// Подключаем вложенный шаблон
        $vars = array('data' => $this->data);
		$this->content = $this->Template( $this->tpl, $vars);
        
		parent::OnOutput();
	}

    public function actionGet(){
        $this->tpl = 'front/index/prep';

        $this->data['prep'] = Prep::Get($_GET['id']);
        $this->title = $this->data['prep']['name'];
    }


} 


