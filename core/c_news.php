<?php
class C_News extends C_Base{
	protected function OnInput(){
        parent::OnInput();

        return $this->GeneratePage($this);
    }
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput(){

		// Подключаем вложенный шаблон
        $vars = array('data' => $this->data);
		$this->content = $this->Template( $this->tpl, $vars);
        
		parent::OnOutput();
	}

    public function actionGet(){
        $this->tpl = 'front/index/news';

        $this->data['news'] = News::Get($_GET['id']);
        $this->title = $this->data['news']['title'];
    }


} 


