<?php
class C_Index extends C_Base{  
	protected function OnInput(){
        parent::OnInput();

        return $this->GeneratePage($this);
    }
	
	//
	// Виртуальный генератор HTML.
	protected function OnOutput(){

		// Подключаем вложенный шаблон
        $vars = array('data' => $this->data);
		$this->content = $this->Template( $this->tpl, $vars);
        
		parent::OnOutput();
	}

	public function actionEmailTest(){
		$title = 'test';
		$message = 'msg test';
		$to = 'yorkshp@gmail.com';
		$from = 'noreply@ash-ram.ru';
		 
		if(Mail::send($to, $title, $message, 'noreply@ash-ram.ru')) 
			echo 'OK';
		else
			echo 'Nope';
		
		die();
	}
    public function actionIndex(){
        $this->tpl = 'front/index/index';
        $this->title = 'Школа медитации Ашрам';

        $this->data['prep'] = Prep::GetAll();
        $this->data['directions'] = Directions::GetAll(true);
        $this->data['sched'] = Sched::GetByDir();
        $this->data['news'] = News::GetAll();
        $this->data['trainings'] = Trainings::GetAll();
    }

    public function actionLogout(){
        Auth::Logout();
        JL::redirect('/');
    }

    public function actionTrainings(){
        $this->tpl = 'front/index/trainings';
        $this->title = 'Тренинги и мастерклассы';

        $this->data['trainings'] = Trainings::GetAll();
    }
    public function actionPage(){
        $this->tpl = 'front/index/page';

        $this->data['page'] = Pages::Get($_GET['id']);

        $this->title = $this->data['page']['title'] . ' | Школа медитации Ashram';

    }
    public function actionDir(){
        $this->tpl = 'front/index/direction';

        $this->data['dir'] = Directions::Get($_GET['id']);

        $this->title = $this->data['dir']['name'] . ' | Школа медитации Ashram';

    }
    public function actionGetPassword(){
        $salt = '8tcYrcz6wQptBVmEVSGDfhzkPTZEQYHt';

        echo sha1(sha1($_GET['pwd'].$salt).SECRET_KEY);
    }
} 


